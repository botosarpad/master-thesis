This is the codebase for the master thesis titled "An ADMM-Based Method for Electricity Spot Market Clearance". It is an implementation of an approach for clearing wholesale, day-ahead electricity spot markets based on the Alternating Direction Method of Multipliers. For details, see the paper.

## Required Software
This project uses Python3, and the following packages:
- [NumPy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [Jupyter Notebook](https://jupyter.org/)
- [GurobiPy](https://www.gurobi.com/)

You can use the following commands to install numpy, pandas and jupyter notebooks:
```
pip3 install numpy
pip3 install pandas
pip3 install notebook
```

Installing gurobi requires a licence and several steps. Follow the offical guide [here](https://support.gurobi.com/hc/en-us/articles/360044290292-How-do-I-install-Gurobi-for-Python).

## Scripts
```
python3 main.py
```
Compares the ADMM approach with the parallel and exact gurobi models on generated data. Results are printed to the command line.

```
python3 iso_ne.py
```
Compares the ADMM approach with the parallel and exact gurobi models on real world, ISO-NE data. Results are printed to the command line.

```
python3 comparison.py
```
The script used to create the data for the graphs in the Benchmarking chapter of the thesis. Results are saved inside the folder `comparison_logging`.

```
python3 hyperparameter_tests.py
```
The script used to create the data for the grid search in the Hyperparameter Selection section of the thesis. Results are saved inside the folder `hyperparameter_optimization`.

## Notebooks

#### Runtime and Welfare Comparison.ipynb
Creates the graphs in the Benchmarking chapter of the thesis based on the data in the folder `comparison_logging`.

#### Hyperparameter Selection.ipynb
Visualizes the results of the hyperparameter selection based on the data in the folder `hyperparameter_optimization`.

#### Iteration Logging.ipynb (Legacy)
Was used to create the graph for convergence behavior in the Stop Condition section of the thesis. This notebook is based on an earlier version of the ADMM model (without the stop condition and parallelization), so it might not work as expected anymore.
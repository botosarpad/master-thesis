import numpy as np
import gurobipy as gp
from gurobipy import GRB
from admm_for_mip.single_hour_admm_mip import Single_Hour_Admm_Mip
from lib.flatten import flatten
from lib.split_by_hour_model import Split_By_Hour_Model
from multiprocessing import Pool

HOURS_OF_DAY = range(24)


class Admm_Mip(Split_By_Hour_Model):
    def __init__(
            self,
            buyer_valuations,
            seller_valuations,
            seller_fixed_costs,
            buyer_bounds,
            seller_bounds,
            seller_min_uptimes,
            sigma=0.6,
            tau=0.6,
            chi=0.6,
            max_iterations=100,
            model_name_prefix="admm"
    ):
        self.max_iterations = max_iterations

        self.solvers = list(map(lambda hour: Single_Hour_Admm_Mip(
            buyer_valuations=buyer_valuations[hour],
            seller_valuations=seller_valuations[hour],
            seller_fixed_costs=seller_fixed_costs[hour],
            buyer_bounds=buyer_bounds[hour],
            seller_bounds=seller_bounds[hour],
            sigma=sigma,
            tau=tau,
            chi=chi,
            model_name_prefix=f"{hour}h_admm"
        ), HOURS_OF_DAY))

        Split_By_Hour_Model.__init__(
            self,
            self.pool_worker,
            buyer_valuations,
            seller_valuations,
            seller_fixed_costs,
            buyer_bounds,
            seller_bounds,
            seller_min_uptimes,
            model_name_prefix
        )

    def pool_worker(self, hour):
        return self.solvers[hour].solve(
            max_iterations=self.max_iterations,
            return_z=True
        )
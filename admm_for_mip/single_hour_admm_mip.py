import numpy as np
import gurobipy as gp
from gurobipy import GRB
from lib.flatten import flatten

np.set_printoptions(precision=2, suppress=True)
# np.set_printoptions(threshold=np.inf)
# np.set_printoptions(linewidth=np.inf)


class Single_Hour_Admm_Mip:
    def __init__(
        self,
        buyer_valuations,
        seller_valuations,
        seller_fixed_costs,
        buyer_bounds,
        seller_bounds,
        sigma=0.6,
        tau=0.6,
        chi=0.6,
        model_name_prefix="single_hour_admm"
    ):
        # Hyperparameters
        self.chi = chi
        self.sigma = sigma
        self.tau = tau
        self.model_name_prefix = model_name_prefix

        # Buyers
        self.buyer_count = len(buyer_valuations)
        self.buyer_valuations = np.array(buyer_valuations)
        self.buyer_bounds = np.array(buyer_bounds)

        # Seller Segments
        self.segment_valuations = np.array(flatten(seller_valuations))
        self.segment_sizes = list(map(lambda sb: np.array(sb[2]), seller_bounds))
        self.segment_counts = list(map(lambda ss: len(ss), self.segment_sizes))
        self.segment_count = sum(self.segment_counts)

        # Sellers
        self.seller_count = len(seller_valuations)
        self.seller_valuations = list(map(lambda sv: np.array(sv), seller_valuations))
        self.fixed_costs = np.array(seller_fixed_costs)
        self.seller_bounds = np.array(list(map(lambda sb: [sb[0], sb[1]], seller_bounds)))
        self.seller_lower_bounds = self.seller_bounds[:, 0]
        self.seller_upper_bounds = self.seller_bounds[:, 1]

        # Other
        self.agent_count = self.buyer_count + self.segment_count
        self.valuations = np.concatenate((self.buyer_valuations, self.segment_valuations))
        self.bounds = np.concatenate((self.buyer_bounds, self.seller_bounds))

    def welfare(self, x, y, z):
        return np.sum(self.buyer_valuations*x) - np.sum(self.segment_valuations*flatten(y)) - np.sum(self.fixed_costs*z)

    # L2 Norm squared
    def nsqr(self, x):
        return np.sum(x**2)

    def lagrangian(self, x, y, z, nu, pi, xi, lower_bound_helpers, upper_bound_helpers, segment_upper_bounds_helpers):
        # The target value to be maximized
        target = self.welfare(x, y, z)

        # Penalty for violating lower bound constraints
        penalty_1 = self.sigma * (self.nsqr(lower_bound_helpers + nu) - self.nsqr(nu)) / 2

        # Penalty for violating upper bound constraints
        penalty_2 = self.tau * (self.nsqr(upper_bound_helpers + pi) - self.nsqr(pi)) / 2

        # Penalty for violating segment upper bounds constraints
        penalty_3 = self.chi * (self.nsqr(
            np.array(flatten(segment_upper_bounds_helpers)) + xi)
            - self.nsqr(xi)) / 2

        return target - penalty_1 - penalty_2 - penalty_3

    # Adapted from https://support.gurobi.com/hc/en-us/articles/15656630439441-How-do-I-use-compute-IIS-to-find-a-subset-of-constraints-that-are-causing-model-infeasibility
    def explain_if_infeasible(self, model):
        if model.Status == GRB.INFEASIBLE:
            model.computeIIS()
            print('\nThe following constraints and variables are in the IIS:')
            for c in model.getConstrs():
                if c.IISConstr:
                    print(f'\t{c.constrname}: {model.getRow(c)} {c.Sense} {c.RHS}')

            for var in model.getVars():
                if var.IISLB:
                    print(f'\t{var.varname} ≥ {var.LB}')
                if var.IISUB:
                    print(f'\t{var.varname} ≤ {var.UB}')

    def initialize_model(self):
        try:
            # CREATE MODEL
            self.m = gp.Model(f"{self.model_name_prefix}_step_1")

            # If model optimization fails, uncomment the following lines to investigate whether it is because
            # of infeasibility or unboundedness.
            # self.m.reset()
            # self.m.setParam("DualReductions", 0)

            # Prevent gurobi from printing unnecessary information to the console
            self.m.Params.LogToConsole = 0

            # DEFINE VARIABLES
            # Define a vector x (and y) for representing each buyer's (seller's) consumption (production)
            x = list(map(lambda i: self.m.addVar(
                vtype=GRB.CONTINUOUS,
                name=f"x{i}"
            ), range(self.buyer_count)))
            y = list(map(lambda i: list(map(lambda j: self.m.addVar(
                vtype=GRB.CONTINUOUS,
                name=f"y{i}_{j}"
            ), range(self.segment_counts[i]))), range(self.seller_count)))

            # Define helper variables for the lower and upper bounds of seller production
            # These variables are needed so the model may temporarily violate these constraints
            # for an increased flexibility.
            lower_bound_helpers = list(map(lambda i: self.m.addVar(
                vtype=GRB.CONTINUOUS,
                name=f"lbh{i}"
            ), range(self.seller_count)))
            upper_bound_helpers = list(map(lambda i: self.m.addVar(
                vtype=GRB.CONTINUOUS,
                name=f"ubh{i}"
            ), range(self.seller_count)))

            # Define helper variables for the upper bounds of segment production
            segment_upper_bounds_helpers = list(map(lambda i: list(map(lambda j: self.m.addVar(
                vtype=GRB.CONTINUOUS,
                name=f"segment_ubh{i}_{j}"
            ), range(self.segment_counts[i]))), range(self.seller_count)))

            # Update the model so we can use the variables we just defined in the following
            self.m.update()

            # SET STATIC CONSTRAINTS (these constraints stay unchanged in each ADMM iteration)
            # Budget balance
            self.m.addConstr(gp.quicksum(x) == gp.quicksum(flatten(y)), name="budget_balance")

            # Buyer consumption lower and upper bounds
            for i in range(self.buyer_count):
                lower_bound = self.bounds[i][0]
                upper_bound = self.bounds[i][1]
                self.m.addConstr(0 <= x[i] - lower_bound, name=f"{x[i].varname}_lower_bound")
                self.m.addConstr(x[i] <= upper_bound, name=f"{x[i].varname}_upper_bound")

            # The list of seller indices, whose constraints have to change in the
            # next iteration step. We initialize it to contain all the sellers, so the
            # constraints are set up during substep1 of the first iteration.
            self.changed_z_indices = range(self.seller_count)

        except gp.GurobiError as e:
            print('Error code ' + str(e.errno) + ': ' + str(e))

        except AttributeError as e:
            print('Encountered an attribute error')
            print(e)

    def substep1(self, z_old, nu_old, pi_old, xi_old, first_iteration):
        try:
            # FETCH MODEL VARIABLES
            x = list(map(lambda i: self.m.getVarByName(
                f"x{i}"
            ), range(self.buyer_count)))
            y = list(map(lambda i: list(map(lambda j: self.m.getVarByName(
                f"y{i}_{j}"
            ), range(self.segment_counts[i]))), range(self.seller_count)))
            lower_bound_helpers = list(map(lambda i: self.m.getVarByName(
                name=f"lbh{i}"
            ), range(self.seller_count)))
            upper_bound_helpers = list(map(lambda i: self.m.getVarByName(
                name=f"ubh{i}"
            ), range(self.seller_count)))
            segment_upper_bounds_helpers = list(map(lambda i: list(map(lambda j: self.m.getVarByName(
                f"segment_ubh{i}_{j}"
            ), range(self.segment_counts[i]))), range(self.seller_count)))

            # UPDATE DYNAMIC CONSTRAINTS
            # Remove all dynamic constraints from the model that have to be changed
            # This step is skipped during the first iteration, because no dynamic constraints
            # have been added before.
            if not first_iteration:
                for i in self.changed_z_indices:
                    self.m.remove(self.m.getConstrByName(f"y{i}_lower_bound"))
                    self.m.remove(self.m.getConstrByName(f"y{i}_upper_bound"))
                    for j in range(self.segment_counts[i]):
                        self.m.remove(self.m.getConstrByName(f"{y[i][j].varname}_upper_bound"))

            # Add the updated dynamic constraints to the model
            # These are necessary, beacuse gurobi cannot work with np.max() in the objective
            for i in self.changed_z_indices:
                self.m.addConstr(lower_bound_helpers[i] >= self.seller_lower_bounds[i] * z_old[i] - gp.quicksum(y[i]), name=f"y{i}_lower_bound")
                self.m.addConstr(upper_bound_helpers[i] >= gp.quicksum(y[i]) - self.seller_upper_bounds[i] * z_old[i], name=f"y{i}_upper_bound")
                for j in range(self.segment_counts[i]):
                    self.m.addConstr(segment_upper_bounds_helpers[i][j] >= y[i][j] - self.segment_sizes[i][j], name=f"{y[i][j].varname}_upper_bound")

            # Reset the variable storing the indices of the sellers whose constraints are to be changed
            self.changed_z_indices = []

            # SET MODEL OBJECTIVE
            self.m.setObjective(self.lagrangian(x, y, z_old, nu_old, pi_old, xi_old,
                                                lower_bound_helpers, upper_bound_helpers, segment_upper_bounds_helpers), GRB.MAXIMIZE)

            # OPTIMIZE MODEL
            self.m.optimize()

            # If the model is infeasible, uncomment this line to get information about which constraints make the model infeasible
            # self.explain_if_infeasible(self.m)

            return list(map(lambda varlist: np.array(list(map(lambda var: var.X, varlist))), y))

        except gp.GurobiError as e:
            print('Error code ' + str(e.errno) + ': ' + str(e))

        except AttributeError as e:
            print('Encountered an attribute error')
            print(e)

    def piecewise_welfare(self, i, y_i, z_i):
        return - np.sum(self.seller_valuations[i]*y_i) - self.fixed_costs[i]*z_i

    def piecewise_lagrangian(self, i, y_i, z_i, nu_i, pi_i, xi_i, lower_bound, upper_bound, segment_sizes):
        # The target value to be maximized
        target = self.piecewise_welfare(i, y_i, z_i)

        # Penalty for violating the lower bound constraint
        penalty_1 = self.sigma * (np.max([0, lower_bound * z_i - np.sum(y_i)] + nu_i)**2 - nu_i**2) / 2

        # Penalty for violating the upper bound constraints
        penalty_2 = self.tau * (np.max([0, np.sum(y_i) - upper_bound * z_i] + pi_i)**2 - pi_i**2) / 2

        # Penalty for violating the segment upper bounds constraint
        penalty_3 = np.sum(
            self.chi * ((np.maximum(0, y_i - segment_sizes * z_i) + xi_i)**2 - xi_i**2) / 2
        )

        return target - penalty_1 - penalty_2 - penalty_3

    def pick_optimal_z(self, i, y_curr, nu_old, pi_old, xi_old):
        # Calculate lagrangian value, if z[i] is set to 1
        one_value = self.piecewise_lagrangian(
            i,
            y_curr,
            1,
            nu_old,
            pi_old,
            xi_old,
            self.seller_lower_bounds[i],
            self.seller_upper_bounds[i],
            self.segment_sizes[i]
        )

        # Calculate lagrangian value if z[i] is set to 0
        zero_value = self.piecewise_lagrangian(
            i,
            y_curr,
            0,
            nu_old,
            pi_old,
            xi_old,
            self.seller_lower_bounds[i],
            self.seller_upper_bounds[i],
            self.segment_sizes[i]
        )

        return 1 if one_value > zero_value else 0

    def substep2(self, y_curr, nu_old, pi_old, xi_old):
        z = np.zeros(self.seller_count)
        for i in range(self.seller_count):
            z[i] = self.pick_optimal_z(i, y_curr[i], nu_old[i], pi_old[i], xi_old[i])
        return z

    def substep3(self, i, y_curr, z_curr, nu_old, pi_old, xi_old):

        # Define some helper variables
        y_sums = np.array(list(map(lambda y_i: np.sum(y_i), y_curr)))
        segment_z = np.concatenate([
            np.zeros(self.segment_counts[j]) if z_curr[j] == 0 else np.ones(self.segment_counts[j]) for j in range(self.seller_count)
        ])

        # ADD the violation of the bounds to the penalty accumulators
        nu_curr = nu_old + np.maximum(0, self.seller_lower_bounds * z_curr - y_sums) * (i+1)
        pi_curr = pi_old + np.maximum(0, y_sums - self.seller_upper_bounds * z_curr) * (i+1)
        xi_curr = xi_old + np.maximum(0, np.array(flatten(y_curr)) -
                                      np.array(flatten(self.segment_sizes)) * segment_z) * (i+1)

        return nu_curr, pi_curr, xi_curr

    def pick_initial_z(self):
        # Solve the linear relaxation of the problem, and round the resulting z_i to 0 or 1
        m = gp.Model(f"{self.model_name_prefix}_linear_relaxation")
        m.Params.LogToConsole = 0

        x = list(map(lambda i: m.addVar(vtype=GRB.CONTINUOUS, name=f"lr_x{i}"), range(self.buyer_count)))
        y = list(map(lambda i: list(map(lambda j: m.addVar(vtype=GRB.CONTINUOUS, name=f"lr_y{i}_{j}"),
                                        range(self.segment_counts[i]))), range(self.seller_count)))
        z = list(map(lambda i: m.addVar(vtype=GRB.CONTINUOUS, ub=1, name=f"lr_z{i}"), range(self.seller_count)))
        m.update()

        m.setObjective(gp.quicksum(self.buyer_valuations * x)
                       - gp.quicksum(self.segment_valuations * flatten(y))
                       - gp.quicksum(self.fixed_costs * z),
                       GRB.MAXIMIZE)

        m.addConstr(gp.quicksum(x) == gp.quicksum(flatten(y)), name="balance")
        for i in range(self.buyer_count):
            lower_bound = self.buyer_bounds[i][0]
            upper_bound = self.buyer_bounds[i][1]
            m.addConstr(0 <= x[i] - lower_bound, name=f"{x[i].varname}_lower_bound")
            m.addConstr(x[i] <= upper_bound, name=f"{x[i].varname}_upper_bound")
        for i in range(self.seller_count):
            for j in range(self.segment_counts[i]):
                upper_bound = self.segment_sizes[i][j]
                m.addConstr(y[i][j] <= upper_bound * z[i], name=f"{y[i][j].varname}_upper_bound")
        for i in range(self.seller_count):
            lower_bound = self.seller_bounds[i][0]
            upper_bound = self.seller_bounds[i][1]
            m.addConstr(0 <= gp.quicksum(y[i]) - lower_bound * z[i], name=f"y{i}_lower_bound")
            m.addConstr(gp.quicksum(y[i]) <= upper_bound * z[i], name=f"y{i}_upper_bound")

        m.optimize()

        return np.round(np.array(list(map(lambda var: var.X, z))))

    def solve_with_fixed_z(self, z):
        # Solve the problem with fixed z
        # This is used to check the feasability of this z
        m = gp.Model(f"{self.model_name_prefix}_fixed_z")
        m.Params.LogToConsole = 0

        x = list(map(lambda i: m.addVar(vtype=GRB.CONTINUOUS, name=f"lr_x{i}"), range(self.buyer_count)))
        y = list(map(lambda i: list(map(lambda j: m.addVar(vtype=GRB.CONTINUOUS, name=f"lr_y{i}_{j}"),
                                        range(self.segment_counts[i]))), range(self.seller_count)))
        m.update()

        m.setObjective(gp.quicksum(self.buyer_valuations * x)
                       - gp.quicksum(self.segment_valuations * flatten(y))
                       - gp.quicksum(self.fixed_costs * z),
                       GRB.MAXIMIZE)

        m.addConstr(gp.quicksum(x) == gp.quicksum(flatten(y)), name="balance")
        for i in range(self.buyer_count):
            lower_bound = self.buyer_bounds[i][0]
            upper_bound = self.buyer_bounds[i][1]
            m.addConstr(0 <= x[i] - lower_bound, name=f"{x[i].varname}_lower_bound")
            m.addConstr(x[i] <= upper_bound, name=f"{x[i].varname}_upper_bound")
        for i in range(self.seller_count):
            for j in range(self.segment_counts[i]):
                upper_bound = self.segment_sizes[i][j]
                m.addConstr(y[i][j] <= upper_bound * z[i], name=f"{y[i][j].varname}_upper_bound")
        for i in range(self.seller_count):
            lower_bound = self.seller_bounds[i][0]
            upper_bound = self.seller_bounds[i][1]
            m.addConstr(0 <= gp.quicksum(y[i]) - lower_bound * z[i], name=f"y{i}_lower_bound")
            m.addConstr(gp.quicksum(y[i]) <= upper_bound * z[i], name=f"y{i}_upper_bound")

        m.optimize()

        # Return empty arrays if the model is not feasible
        if m.Status != GRB.OPTIMAL:
            return [], []

        return np.array(list(map(lambda var: var.X, x))), list(map(lambda varlist: np.array(list(map(lambda var: var.X, varlist))), y))

    def solve(self, max_iterations=100, log_file=None, return_z=False):
        # INITIALIZE MODEL AND VARIABLES
        z = self.pick_initial_z()
        nu = np.zeros(self.seller_count)
        pi = np.zeros(self.seller_count)
        xi = np.zeros(self.segment_count)
        self.initialize_model()

        # Open log file if needed
        if log_file is not None:
            output_file = open(log_file, "w")

        # ADMM ITERATIONS
        for i in range(max_iterations):
            # Save the old z so we can later tell how much it has changed during this iteration step
            old_z = z

            # Fix z, optimize for x and y
            y = self.substep1(z, nu, pi, xi, i == 0)

            # Log current values, if needed
            if log_file is not None:
                output_file.write(f"{flatten(y)}\n")

            # Fix y, optimize for z
            z = self.substep2(y, nu, pi, xi)

            # For each z that has changed, the dynamic seller constraints will have to be updated in the next step,
            # therefore we take note of the relevant seller indices
            for j in range(self.seller_count):
                if z[j] != old_z[j]:
                    self.changed_z_indices.append(j)

            # Update penalty accumulators
            nu, pi, xi = self.substep3(i, y, z, nu, pi, xi)

            # Break condition
            if i == max_iterations - 1:
                print("Reached max iterations")
            if len(self.changed_z_indices) == 0:
                x, y = self.solve_with_fixed_z(z)
                # If z is infeasible, and empty arrays are returned
                if len(x) != 0 and len(y) != 0:
                    break

        if return_z:
            return z

        # Solve the precise (non-lagrangian) model with the calculated z from the iterations
        x, y = self.solve_with_fixed_z(z)

        # Add results and close log file if needed
        if log_file is not None:
            output_file.write(f"{np.concatenate((x,flatten(y))).tolist()}\n")
            output_file.close()

        return np.concatenate((x, flatten(y)))

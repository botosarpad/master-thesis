import time
from data_importers.iso_ne_importer import import_data
from gurobi_solver.exact_gurobi_mip import Exact_Gurobi_Mip
from gurobi_solver.parallelized_gurobi_mip import Parallelized_Gurobi_Mip
from admm_for_mip.admm_mip import Admm_Mip
from lib.welfare import welfare
from examples.dotdict import dotdict
from lib.constraint_violation import count_violated_constraints, count_violated_temporal_constraints
from examples.random_minimum_uptime import add_random_minimum_uptimes
from examples.full_day_examples import get_examples


def compare(log_file):
    exact_gurobi_utilities = []
    exact_gurobi_times = []
    exact_gurobi_vcs = []
    exact_gurobi_vtcs = []
    parallelized_gurobi_utilities = []
    parallelized_gurobi_times = []
    parallelized_gurobi_vcs = []
    parallelized_gurobi_vtcs = []
    admm_utilities = []
    admm_times = []
    admm_vcs = []
    admm_vtcs = []

    for ex in examples:
        exact_gurobi = Exact_Gurobi_Mip(
            buyer_valuations=ex["buyer_valuations"],
            seller_valuations=ex["seller_valuations"],
            seller_fixed_costs=ex["seller_fixed_costs"],
            buyer_bounds=ex["buyer_bounds"],
            seller_bounds=ex["seller_bounds"],
            seller_min_uptimes=ex["seller_min_uptimes"]
        )
        exact_gurobi_start_time = time.perf_counter()
        exact_gurobi_solution = exact_gurobi.solve()
        exact_gurobi_end_time = time.perf_counter()
        exact_gurobi_welfare = welfare(exact_gurobi_solution, ex)
        exact_gurobi_violated_constraints = count_violated_constraints(exact_gurobi_solution, ex)
        exact_gurobi_violated_temp_constraints = count_violated_temporal_constraints(exact_gurobi_solution, ex)
        exact_gurobi_time = exact_gurobi_end_time - exact_gurobi_start_time
        exact_gurobi_utilities.append(exact_gurobi_welfare)
        exact_gurobi_times.append(exact_gurobi_time)
        exact_gurobi_vcs.append(exact_gurobi_violated_constraints)
        exact_gurobi_vtcs.append(exact_gurobi_violated_temp_constraints)

        parallelized_gurobi = Parallelized_Gurobi_Mip(
            buyer_valuations=ex["buyer_valuations"],
            seller_valuations=ex["seller_valuations"],
            seller_fixed_costs=ex["seller_fixed_costs"],
            buyer_bounds=ex["buyer_bounds"],
            seller_bounds=ex["seller_bounds"],
            seller_min_uptimes=ex["seller_min_uptimes"]
        )
        parallelized_gurobi_start_time = time.perf_counter()
        parallelized_gurobi_solution = parallelized_gurobi.solve()
        parallelized_gurobi_end_time = time.perf_counter()
        parallelized_gurobi_welfare = welfare(parallelized_gurobi_solution, ex)
        parallelized_gurobi_violated_constraints = count_violated_constraints(parallelized_gurobi_solution, ex)
        parallelized_gurobi_violated_temp_constraints = count_violated_temporal_constraints(
            parallelized_gurobi_solution, ex)
        parallelized_gurobi_time = parallelized_gurobi_end_time - parallelized_gurobi_start_time
        parallelized_gurobi_utilities.append(parallelized_gurobi_welfare)
        parallelized_gurobi_times.append(parallelized_gurobi_time)
        parallelized_gurobi_vcs.append(parallelized_gurobi_violated_constraints)
        parallelized_gurobi_vtcs.append(parallelized_gurobi_violated_temp_constraints)

        admm = Admm_Mip(
            buyer_valuations=ex.buyer_valuations,
            seller_valuations=ex.seller_valuations,
            seller_fixed_costs=ex.seller_fixed_costs,
            buyer_bounds=ex.buyer_bounds,
            seller_bounds=ex.seller_bounds,
            seller_min_uptimes=ex.seller_min_uptimes,
            max_iterations=30
        )
        admm_start_time = time.perf_counter()
        admm_solution = admm.solve()
        admm_end_time = time.perf_counter()
        admm_welfare = welfare(admm_solution, ex)
        admm_violated_constraints = count_violated_constraints(admm_solution, ex)
        admm_violated_temp_constraints = count_violated_temporal_constraints(admm_solution, ex)
        admm_time = admm_end_time - admm_start_time
        admm_utilities.append(admm_welfare)
        admm_times.append(admm_time)
        admm_vcs.append(admm_violated_constraints)
        admm_vtcs.append(admm_violated_temp_constraints)

    output_file = open(log_file, "w")
    output_file.write(f"{exact_gurobi_utilities}\n")
    output_file.write(f"{exact_gurobi_times}\n")
    output_file.write(f"{exact_gurobi_vcs}\n")
    output_file.write(f"{exact_gurobi_vtcs}\n")
    output_file.write(f"{parallelized_gurobi_utilities}\n")
    output_file.write(f"{parallelized_gurobi_times}\n")
    output_file.write(f"{parallelized_gurobi_vcs}\n")
    output_file.write(f"{parallelized_gurobi_vtcs}\n")
    output_file.write(f"{admm_utilities}\n")
    output_file.write(f"{admm_times}\n")
    output_file.write(f"{admm_vcs}\n")
    output_file.write(f"{admm_vtcs}\n")
    output_file.close()


demand_sizes = ["2500", "5000", "7500", "10000", "full"]
offer_sizes = ["2000", "4000", "6000", "8000", "full"]
examples = []
for i in range(len(demand_sizes)):
    ex = dotdict(import_data(demand_file=f"data_importers/data/variable_size_examples/demand_{demand_sizes[i]}.csv",
                             offer_file=f"data_importers/data/variable_size_examples/offer_{offer_sizes[i]}.csv"))
    ex = dotdict(add_random_minimum_uptimes(ex))
    examples.append(ex)
compare("comparison_logging/iso_ne")

examples = list(get_examples())
compare("comparison_logging/generated")

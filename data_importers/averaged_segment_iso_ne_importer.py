import pandas as pd


def import_demand_data(demand_data, log_stats=False):
    hour_of_day = []
    buyer_ids = []
    buyer_valuations = []
    buyer_bounds = []
    statistics = {
        "PRICE": 0,
        "DEC": 0,
        "INC": 0,
        "FIXED": 0
    }

    for index, row in demand_data.iterrows():
        bid_type = row["Bid Type"]

        if log_stats:
            statistics[bid_type] += 1

        if bid_type == "DEC" or bid_type == "INC":
            continue

        hour_of_day.append(row["Hour"])
        buyer_ids.append(row["Masked Lead Participant ID"])
        if bid_type == "FIXED":
            bound = row["Segment 1 MW"]
            buyer_bounds.append([bound, bound])
            buyer_valuations.append(0)
        if bid_type == "PRICE":
            for i in range(1, 51):
                segment_mw = row[f"Segment {i} MW"]
                segment_price = row[f"Segment {i} Price"]
                if pd.isna(segment_mw) or pd.isna(segment_price):
                    break
                buyer_bounds.append([0, segment_mw])
                buyer_valuations.append(segment_price)

    if log_stats:
        print(statistics)

    return hour_of_day, buyer_ids, buyer_bounds, buyer_valuations


def import_offer_data(offer_data, log_stats=False):
    hour_of_day = []
    seller_ids = []
    seller_valuations = []
    seller_bounds = []
    seller_fixed_costs = []
    statistics = {
        "ECONOMIC": 0,
        "MUST_RUN": 0,
        "UNAVAILABLE": 0
    }

    for index, row in offer_data.iterrows():
        unit_status = row["Unit Status"]

        if log_stats:
            statistics[unit_status] += 1

        if unit_status == "UNAVAILABLE" or unit_status == "MUST_RUN":
            continue

        hour_of_day.append(row["Trading Interval"])
        seller_ids.append(row["Masked Asset ID"])
        if unit_status == "ECONOMIC":
            seller_bounds.append([row["Economic Minimum"], row["Economic Maximum"]])
            seller_fixed_costs.append(row["Hot Startup Price"])
            valuation = 0
            for i in range(1, 10):
                segment_price = row[f"Segment {i} Price"]
                if pd.isna(segment_price):
                    valuation /= i - 1
                    break
                else:
                    valuation += segment_price
            seller_valuations.append(valuation)

    if log_stats:
        print(statistics)

    return hour_of_day, seller_ids, seller_bounds, seller_valuations, seller_fixed_costs


def import_data_with_averaged_segments(offer_file, demand_file):
    offer_data = pd.read_csv(offer_file)
    _, _, seller_bounds, seller_valuations, seller_fixed_costs = import_offer_data(offer_data)
    demand_data = pd.read_csv(demand_file)
    _, _, buyer_bounds, buyer_valuations = import_demand_data(demand_data)
    return {
        'name': 'ISO-NE-Imported-Data',
        'buyer_valuations': buyer_valuations,
        'seller_valuations': seller_valuations,
        'seller_fixed_costs': seller_fixed_costs,
        'buyer_bounds': buyer_bounds,
        'seller_bounds': seller_bounds
    }

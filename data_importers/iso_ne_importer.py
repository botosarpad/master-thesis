import pandas as pd
from collections import Counter


def import_demand_data(demand_data, log_stats=False):
    hour_of_day = []
    buyer_ids = []
    buyer_valuations = []
    buyer_bounds = []
    statistics = {
        "PRICE": 0,
        "DEC": 0,
        "INC": 0,
        "FIXED": 0
    }

    for index, row in demand_data.iterrows():
        bid_type = row["Bid Type"]

        if log_stats:
            statistics[bid_type] += 1

        if bid_type == "DEC" or bid_type == "INC":
            continue

        if bid_type == "FIXED":
            hour_of_day.append(row["Hour"])
            buyer_ids.append(row["Masked Lead Participant ID"])
            bound = row["Segment 1 MW"]
            buyer_bounds.append([bound, bound])
            buyer_valuations.append(0)
        if bid_type == "PRICE":
            for i in range(1, 51):
                segment_mw = row[f"Segment {i} MW"]
                segment_price = row[f"Segment {i} Price"]
                if pd.isna(segment_mw) or pd.isna(segment_price):
                    break
                hour_of_day.append(row["Hour"])
                buyer_ids.append(row["Masked Lead Participant ID"])
                buyer_bounds.append([0, segment_mw])
                buyer_valuations.append(segment_price)

    if log_stats:
        print(statistics)

    return hour_of_day, buyer_ids, buyer_bounds, buyer_valuations


def import_offer_data(offer_data, log_stats=False):
    hour_of_day = []
    seller_ids = []
    seller_valuations = []
    seller_bounds = []
    seller_fixed_costs = []
    statistics = {
        "ECONOMIC": 0,
        "MUST_RUN": 0,
        "UNAVAILABLE": 0
    }

    for index, row in offer_data.iterrows():
        unit_status = row["Unit Status"]

        if log_stats:
            statistics[unit_status] += 1

        if unit_status == "UNAVAILABLE" or unit_status == "MUST_RUN":
            continue

        hour_of_day.append(row["Trading Interval"])
        seller_ids.append(row["Masked Asset ID"])
        if unit_status == "ECONOMIC":
            seller_fixed_costs.append(row["No Load Price"])
            segment_sizes = []
            segment_prices = []
            for i in range(1, 10):
                segment_price = row[f"Segment {i} Price"]
                segment_size = row[f"Segment {i} MW"]
                if pd.isna(segment_price):
                    break
                else:
                    segment_prices.append(segment_price)
                    segment_sizes.append(segment_size)
            seller_bounds.append([row["Economic Minimum"], row["Economic Maximum"], segment_sizes])
            seller_valuations.append(segment_prices)

    if log_stats:
        print(statistics)

    return hour_of_day, seller_ids, seller_bounds, seller_valuations, seller_fixed_costs


def import_data(offer_file, demand_file):
    offer_data = pd.read_csv(offer_file)
    seller_hour_of_day, seller_ids, seller_bounds, seller_valuations, seller_fixed_costs = import_offer_data(offer_data)
    demand_data = pd.read_csv(demand_file)
    buyer_hour_of_day, buyer_ids, buyer_bounds, buyer_valuations = import_demand_data(demand_data)

    buyer_data_len = len(buyer_hour_of_day)
    seller_data_len = len(seller_hour_of_day)

    buyer_valuations_per_hour = [[] for _ in range(24)]
    buyer_bounds_per_hour = [[] for _ in range(24)]
    for i in range(buyer_data_len):
        hour = buyer_hour_of_day[i] - 1
        buyer_valuations_per_hour[hour].append(buyer_valuations[i])
        buyer_bounds_per_hour[hour].append(buyer_bounds[i])

    seller_valuations_per_hour = [[] for _ in range(24)]
    seller_fixed_costs_per_hour = [[] for _ in range(24)]
    seller_bounds_per_hour = [[] for _ in range(24)]
    for i in range(seller_data_len):
        hour = seller_hour_of_day[i] - 1
        seller_valuations_per_hour[hour].append(seller_valuations[i])
        seller_fixed_costs_per_hour[hour].append(seller_fixed_costs[i])
        seller_bounds_per_hour[hour].append(seller_bounds[i])

    return {
        'name': 'ISO-NE-Imported-Data',
        'buyer_valuations': buyer_valuations_per_hour,
        'seller_valuations': seller_valuations_per_hour,
        'seller_fixed_costs': seller_fixed_costs_per_hour,
        'buyer_bounds': buyer_bounds_per_hour,
        'seller_bounds': seller_bounds_per_hour
    }

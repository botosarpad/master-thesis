from dotdict import dotdict
import numpy as np
import numpy.random as npr
from uuid import uuid4
from random_minimum_uptime import add_random_minimum_uptimes


def random_interval():
    lower_bound = npr.randint(1, 1000)
    upper_bound = npr.randint(lower_bound, 3 * lower_bound)
    return [lower_bound, upper_bound]


def generate_simple_example(buyer_count=npr.randint(1, 200), seller_count=npr.randint(1, 200)):
    feasible = False
    attempt = 1
    while not feasible:
        print("Attempt", attempt)
        buyer_valuations = npr.randint(1, 10000, buyer_count).tolist()
        seller_valuations = npr.randint(1, 10000, seller_count).tolist()
        seller_fixed_costs = npr.randint(0, 100000, seller_count).tolist()
        buyer_bounds = np.array(list(map(lambda _: random_interval(), range(buyer_count))))
        seller_bounds = np.array(list(map(lambda _: random_interval(), range(seller_count))))
        # Test if the generated example is feasible
        min_consumption = sum(buyer_bounds[:, 0])
        max_consumption = sum(buyer_bounds[:, 1])
        min_production = sum(seller_bounds[:, 0])
        max_production = sum(seller_bounds[:, 1])
        feasible = min_consumption <= max_production and min_production <= max_consumption
        attempt += 1
    return dotdict({
        "name": f"Example {uuid4()}",
        "buyer_valuations": buyer_valuations,
        "seller_valuations": seller_valuations,
        "seller_fixed_costs": seller_fixed_costs,
        "buyer_bounds": buyer_bounds.tolist(),
        "seller_bounds": seller_bounds.tolist()
    })


def generate_segment_example(buyer_count=npr.randint(1, 50), seller_count=npr.randint(1, 50), max_segment_count=20):
    feasible = False
    attempt = 1
    while not feasible:
        print("Attempt", attempt)
        # Generate buyer data
        buyer_valuations = npr.randint(1, 1000, buyer_count).tolist()
        buyer_bounds = np.array(list(map(lambda _: random_interval(), range(buyer_count))))

        # Generate seller data
        seller_valuations = []
        seller_bounds = []
        for i in range(seller_count):
            segment_count = npr.randint(1, max_segment_count)
            segment_prices = np.sort(npr.randint(1, 1000, segment_count)).tolist()
            seller_valuations.append(segment_prices)
            seller_bound = random_interval()
            segment_boundaries = np.sort(npr.randint(0, seller_bound[1], segment_count - 1)).tolist()
            segment_boundaries.append(seller_bound[1])
            segment_sizes = [segment_boundaries[0]]
            for j in range(1, segment_count):
                segment_sizes.append(segment_boundaries[j] - segment_boundaries[j-1])
            seller_bounds.append([seller_bound[0], seller_bound[1], segment_sizes])
        seller_fixed_costs = npr.randint(0, 1000, seller_count).tolist()

        # Test if the generated example is feasible
        min_consumption = sum(buyer_bounds[:, 0])
        max_consumption = sum(buyer_bounds[:, 1])
        min_production = sum(list(map(lambda sb: sb[0], seller_bounds)))
        max_production = sum(list(map(lambda sb: sb[1], seller_bounds)))
        feasible = min_consumption <= max_production and min_production <= max_consumption
        attempt += 1
    return dotdict({
        "name": f"Example {uuid4()}",
        "buyer_valuations": buyer_valuations,
        "seller_valuations": seller_valuations,
        "seller_fixed_costs": seller_fixed_costs,
        "buyer_bounds": buyer_bounds.tolist(),
        "seller_bounds": seller_bounds
    })


def generate_24h_example(buyer_count=npr.randint(1, 10), seller_count=npr.randint(1, 10), max_segment_count=20):
    example = dotdict({
        "name": f"Example {uuid4()}",
        "buyer_valuations": [],
        "seller_valuations": [],
        "seller_fixed_costs": [],
        "buyer_bounds": [],
        "seller_bounds": []
    })
    for hour in range(24):
        ex = generate_segment_example(buyer_count, seller_count, max_segment_count)
        example.buyer_valuations.append(ex.buyer_valuations)
        example.seller_valuations.append(ex.seller_valuations)
        example.seller_fixed_costs.append(ex.seller_fixed_costs)
        example.buyer_bounds.append(ex.buyer_bounds)
        example.seller_bounds.append(ex.seller_bounds)
    return add_random_minimum_uptimes(example)


print(generate_24h_example(buyer_count=10, seller_count=10))

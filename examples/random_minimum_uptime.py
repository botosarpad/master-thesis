import numpy.random as npr


def add_random_minimum_uptimes(example, seed=1234):
    npr.seed(seed)
    seller_count = len(example.seller_valuations[0])
    return {
        **example,
        'seller_min_uptimes': npr.randint(0, 24, seller_count).tolist()
    }

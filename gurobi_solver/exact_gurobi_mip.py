import numpy as np
import gurobipy as gp
from gurobipy import GRB
from lib.flatten import flatten

HOURS_OF_DAY = range(24)


class Exact_Gurobi_Mip:
    def __init__(
        self,
        buyer_valuations,
        seller_valuations,
        seller_fixed_costs,
        buyer_bounds,
        seller_bounds,
        seller_min_uptimes,
        model_name_prefix="exact_gurobi"
    ):
        # Buyers
        self.buyer_counts = list(map(lambda bv: len(bv), buyer_valuations))
        self.buyer_valuations = list(map(lambda bv: np.array(bv), buyer_valuations))
        self.buyer_bounds = list(map(lambda bb: np.array(bb), buyer_bounds))

        # Seller Segments
        self.segment_valuations = list(map(lambda sv: np.array(flatten(sv)), seller_valuations))
        self.segment_sizes = list(map(lambda sb: list(map(lambda bounds: np.array(bounds[2]), sb)), seller_bounds))
        self.segment_counts = list(map(lambda ss: list(map(lambda sizes: len(sizes), ss)), self.segment_sizes))

        # Sellers
        self.seller_count = len(seller_valuations[0])  # Should be the same for all hours, so we take the first hour
        self.seller_fixed_costs = list(map(lambda sfc: np.array(sfc), seller_fixed_costs))
        self.seller_bounds = list(map(lambda sb: np.array(
            list(map(lambda bounds: [bounds[0], bounds[1]], sb))), seller_bounds))
        self.seller_min_uptimes = np.array(seller_min_uptimes)

        self.model_name_prefix = model_name_prefix

    def solve(self):
        m = gp.Model(f"{self.model_name_prefix}_model")
        m.Params.LogToConsole = 0

        x = list(map(lambda hour:
                     list(map(lambda i: m.addVar(vtype=GRB.CONTINUOUS, name=f"{hour}h_x{i}"), range(self.buyer_counts[hour]))),
                     HOURS_OF_DAY))
        y = list(map(lambda hour:
                     list(map(lambda i:
                              list(map(lambda j: m.addVar(vtype=GRB.CONTINUOUS, name=f"{hour}h_y{i}_{j}"), range(self.segment_counts[hour][i]))),
                              range(self.seller_count))),
                     HOURS_OF_DAY))
        z = list(map(lambda hour:
                     list(map(lambda i: m.addVar(vtype=GRB.BINARY, name=f"{hour}h_z{i}"), range(self.seller_count))),
                     HOURS_OF_DAY))
        phi = list(map(lambda hour:
                       list(map(lambda i: m.addVar(vtype=GRB.CONTINUOUS, name=f"{hour}h_phi{i}"), range(self.seller_count))),
                       HOURS_OF_DAY))
        m.update()

        objective = 0
        for hour in HOURS_OF_DAY:
            objective += gp.quicksum(self.buyer_valuations[hour] * x[hour]) - gp.quicksum(
                self.segment_valuations[hour] * flatten(y[hour])) - gp.quicksum(self.seller_fixed_costs[hour] * z[hour])
        m.setObjective(objective, GRB.MAXIMIZE)

        for hour in HOURS_OF_DAY:
            m.addConstr(gp.quicksum(x[hour]) == gp.quicksum(flatten(y[hour])), name=f"{hour}h_balance")
            for i in range(self.buyer_counts[hour]):
                lower_bound = self.buyer_bounds[hour][i][0]
                upper_bound = self.buyer_bounds[hour][i][1]
                m.addConstr(0 <= x[hour][i] - lower_bound, name=f"{x[hour][i].varname}_lower_bound")
                m.addConstr(x[hour][i] <= upper_bound, name=f"{x[hour][i].varname}_upper_bound")
            for i in range(self.seller_count):
                for j in range(self.segment_counts[hour][i]):
                    upper_bound = self.segment_sizes[hour][i][j]
                    m.addConstr(y[hour][i][j] <= upper_bound * z[hour][i], name=f"{y[hour][i][j].varname}_upper_bound")
            for i in range(self.seller_count):
                lower_bound = self.seller_bounds[hour][i][0]
                upper_bound = self.seller_bounds[hour][i][1]
                m.addConstr(0 <= gp.quicksum(y[hour][i]) - lower_bound * z[hour][i], name=f"{hour}h_y{i}_lower_bound")
                m.addConstr(gp.quicksum(y[hour][i]) <= upper_bound * z[hour][i], name=f"{hour}h_y{i}_upper_bound")

        for hour in HOURS_OF_DAY:
            for i in range(self.seller_count):
                if hour == 0:
                    m.addConstr(phi[hour][i] - z[hour][i] >= 0)
                else:
                    m.addConstr(phi[hour][i] - z[hour][i] + z[hour-1][i] >= 0, name=f"{phi[hour][i].varname}_startup_indicator")
                earliest_relevant_hour = np.maximum(0, hour - self.seller_min_uptimes[i] + 1)
                phi_sum = 0
                for j in range(earliest_relevant_hour, hour + 1):
                    phi_sum += phi[j][i]
                m.addConstr(phi_sum - z[hour][i] <= 0, name=f"y{i}_min_uptime")

        m.optimize()

        return list(map(lambda hour:
                        np.concatenate((np.array(list(map(lambda var: var.X, x[hour]))),
                                        np.array(list(map(lambda var: var.X, flatten(y[hour])))))),
                        HOURS_OF_DAY))

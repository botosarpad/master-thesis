import numpy as np
import gurobipy as gp
from gurobipy import GRB
from multiprocessing import Pool
from gurobi_solver.single_hour_gurobi_mip import Single_Hour_Gurobi_Mip
from lib.flatten import flatten
from lib.split_by_hour_model import Split_By_Hour_Model

HOURS_OF_DAY = range(24)


class Parallelized_Gurobi_Mip(Split_By_Hour_Model):
    def __init__(
            self,
            buyer_valuations,
            seller_valuations,
            seller_fixed_costs,
            buyer_bounds,
            seller_bounds,
            seller_min_uptimes,
            model_name_prefix="parallelized_gurobi"
    ):
        self.solvers = list(map(lambda hour: Single_Hour_Gurobi_Mip(
            buyer_valuations=buyer_valuations[hour],
            seller_valuations=seller_valuations[hour],
            seller_fixed_costs=seller_fixed_costs[hour],
            buyer_bounds=buyer_bounds[hour],
            seller_bounds=seller_bounds[hour],
            model_name_prefix=f"{hour}h_gurobi"
        ), HOURS_OF_DAY))

        Split_By_Hour_Model.__init__(
            self,
            self.pool_worker,
            buyer_valuations,
            seller_valuations,
            seller_fixed_costs,
            buyer_bounds,
            seller_bounds,
            seller_min_uptimes,
            model_name_prefix
        )

    def pool_worker(self, hour):
        return self.solvers[hour].solve(return_z=True)

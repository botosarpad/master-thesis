import numpy as np
import gurobipy as gp
from gurobipy import GRB
from lib.flatten import flatten


class Single_Hour_Gurobi_Mip:
    def __init__(
        self,
        buyer_valuations,
        seller_valuations,
        seller_fixed_costs,
        buyer_bounds,
        seller_bounds,
        model_name_prefix="single_hour_gurobi"
    ):
        # Buyers
        self.buyer_count = len(buyer_valuations)
        self.buyer_valuations = np.array(buyer_valuations)
        self.buyer_bounds = np.array(buyer_bounds)

        # Seller Segments
        self.segment_valuations = np.array(flatten(seller_valuations))
        self.segment_sizes = list(map(lambda sb: np.array(sb[2]), seller_bounds))
        self.segment_counts = list(map(lambda ss: len(ss), self.segment_sizes))
        self.segment_count = sum(self.segment_counts)

        # Sellers
        self.seller_count = len(seller_valuations)
        self.seller_fixed_costs = np.array(seller_fixed_costs)
        self.seller_bounds = np.array(list(map(lambda sb: [sb[0], sb[1]], seller_bounds)))

        self.agent_count = self.buyer_count + self.segment_count
        self.model_name_prefix = model_name_prefix

    def solve(self, return_z=False):
        m = gp.Model(f"{self.model_name_prefix}_model")
        m.Params.LogToConsole = 0

        x = list(map(lambda i: m.addVar(vtype=GRB.CONTINUOUS, name=f"x{i}"), range(self.buyer_count)))
        y = list(map(lambda i: list(map(lambda j: m.addVar(vtype=GRB.CONTINUOUS, name=f"y{i}_{j}"),
                                        range(self.segment_counts[i]))), range(self.seller_count)))
        z = list(map(lambda i: m.addVar(vtype=GRB.BINARY, name=f"z{i}"), range(self.seller_count)))
        m.update()

        m.setObjective(gp.quicksum(self.buyer_valuations * x)
                       - gp.quicksum(self.segment_valuations * flatten(y))
                       - gp.quicksum(self.seller_fixed_costs * z),
                       GRB.MAXIMIZE)

        m.addConstr(gp.quicksum(x) == gp.quicksum(flatten(y)), name="balance")
        for i in range(self.buyer_count):
            lower_bound = self.buyer_bounds[i][0]
            upper_bound = self.buyer_bounds[i][1]
            m.addConstr(0 <= x[i] - lower_bound, name=f"{x[i].varname}_lower_bound")
            m.addConstr(x[i] <= upper_bound, name=f"{x[i].varname}_upper_bound")
        for i in range(self.seller_count):
            for j in range(self.segment_counts[i]):
                upper_bound = self.segment_sizes[i][j]
                m.addConstr(y[i][j] <= upper_bound * z[i], name=f"{y[i][j].varname}_upper_bound")
        for i in range(self.seller_count):
            lower_bound = self.seller_bounds[i][0]
            upper_bound = self.seller_bounds[i][1]
            m.addConstr(0 <= gp.quicksum(y[i]) - lower_bound * z[i], name=f"y{i}_lower_bound")
            m.addConstr(gp.quicksum(y[i]) <= upper_bound * z[i], name=f"y{i}_upper_bound")

        m.optimize()

        if return_z:
            return np.array(list(map(lambda var: var.X, z)))
        return np.concatenate((np.array(list(map(lambda var: var.X, x))), np.array(list(map(lambda var: var.X, flatten(y))))))

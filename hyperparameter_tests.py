import numpy as np
import time
from examples.hyperparameter_optimization_examples import get_examples
from admm_for_mip.admm_mip import Admm_Mip
from gurobi_solver.exact_gurobi_mip import Exact_Gurobi_Mip
from lib.welfare import welfare
from lib.constraint_violation import count_violated_constraints, total_constraint_violation_extent, count_violated_temporal_constraints

np.set_printoptions(precision=2, suppress=True)
np.set_printoptions(threshold=np.inf)
np.set_printoptions(linewidth=np.inf)


def export_gurobi_solutions(examples, log_file):
    example_names = []
    gurobi_utilities = []
    gurobi_times = []
    gurobi_vcs = []
    gurobi_vtcs = []
    for ex in examples:
        gurobi = Exact_Gurobi_Mip(
            buyer_valuations=ex.buyer_valuations,
            seller_valuations=ex.seller_valuations,
            seller_fixed_costs=ex.seller_fixed_costs,
            buyer_bounds=ex.buyer_bounds,
            seller_bounds=ex.seller_bounds,
            seller_min_uptimes=ex.seller_min_uptimes
        )
        gurobi_start_time = time.perf_counter()
        gurobi_solution = gurobi.solve()
        gurobi_end_time = time.perf_counter()

        example_names.append(ex.name)
        gurobi_utilities.append(welfare(gurobi_solution, ex))
        gurobi_times.append(gurobi_end_time - gurobi_start_time)
        gurobi_vcs.append(count_violated_constraints(gurobi_solution, ex))
        gurobi_vtcs.append(count_violated_temporal_constraints(gurobi_solution, ex))

    output_file = open(log_file, "w")
    output_file.write(f"{example_names}\n")
    output_file.write(f"{gurobi_utilities}\n")
    output_file.write(f"{gurobi_times}\n")
    output_file.write(f"{gurobi_vcs}\n")
    output_file.write(f"{gurobi_vtcs}\n")
    output_file.close()


def export_admm_solutions(examples, sigma_range, tau_chi_range, log_folder):
    example_names = [[["" for example in examples] for tau_chi in tau_chi_range] for sigma in sigma_range]
    admm_utilities = np.zeros((len(sigma_range), len(tau_chi_range), len(examples)))
    admm_times = np.zeros((len(sigma_range), len(tau_chi_range), len(examples)))
    admm_vcs = np.zeros((len(sigma_range), len(tau_chi_range), len(examples)))
    admm_vtcs = np.zeros((len(sigma_range), len(tau_chi_range), len(examples)))
    for i, sigma in enumerate(sigma_range):
        print(f"sigma = {sigma}")
        for j, tau_chi in enumerate(tau_chi_range):
            print(f"\ttau_chi = {tau_chi}")
            for k, ex in enumerate(examples):
                admm = Admm_Mip(
                    buyer_valuations=ex.buyer_valuations,
                    seller_valuations=ex.seller_valuations,
                    seller_fixed_costs=ex.seller_fixed_costs,
                    buyer_bounds=ex.buyer_bounds,
                    seller_bounds=ex.seller_bounds,
                    seller_min_uptimes=ex.seller_min_uptimes,
                    sigma=sigma,
                    tau=tau_chi,
                    chi=tau_chi,
                )
                admm_start_time = time.perf_counter()
                admm_solution = admm.solve()
                admm_end_time = time.perf_counter()

                example_names[i][j][k] = ex.name
                admm_utilities[i][j][k] = welfare(admm_solution, ex)
                admm_times[i][j][k] = admm_end_time - admm_start_time
                admm_vcs[i][j][k] = count_violated_constraints(admm_solution, ex)
                admm_vtcs[i][j][k] = count_violated_temporal_constraints(admm_solution, ex)

    np.save(f"{log_folder}/admm_utilities", admm_utilities)
    np.save(f"{log_folder}/admm_times", admm_times)
    np.save(f"{log_folder}/admm_vcs", admm_vcs)
    np.save(f"{log_folder}/admm_vtcs", admm_vtcs)


examples = list(get_examples())
export_gurobi_solutions(examples, "hyperparameter_optimization/gurobi")

sigma_range = np.arange(0.1, 1, 0.1)
tau_chi_range = np.arange(0.1, 1, 0.1)
print(sigma_range)
export_admm_solutions(examples, sigma_range, tau_chi_range, "hyperparameter_optimization")

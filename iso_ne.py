import numpy as np
import time
from data_importers.iso_ne_importer import import_data
from gurobi_solver.exact_gurobi_mip import Exact_Gurobi_Mip
from gurobi_solver.parallelized_gurobi_mip import Parallelized_Gurobi_Mip
from admm_for_mip.admm_mip import Admm_Mip
from lib.welfare import welfare
from examples.dotdict import dotdict
from examples.random_minimum_uptime import add_random_minimum_uptimes


ex = dotdict(import_data(demand_file="data_importers/data/iso_ne_demand.csv",
                         offer_file="data_importers/data/iso_ne_offer_processed.csv"))
print("Data imported")
ex = dotdict(add_random_minimum_uptimes(ex))

gurobi = Exact_Gurobi_Mip(
    buyer_valuations=ex["buyer_valuations"],
    seller_valuations=ex["seller_valuations"],
    seller_fixed_costs=ex["seller_fixed_costs"],
    buyer_bounds=ex["buyer_bounds"],
    seller_bounds=ex["seller_bounds"],
    seller_min_uptimes=ex["seller_min_uptimes"]
)
gurobi_start_time = time.perf_counter()
gurobi_solution = gurobi.solve()
gurobi_end_time = time.perf_counter()
gurobi_welfare = welfare(gurobi_solution, ex)
print("Exact Gurobi Solution:")
print("welfare:", np.round(gurobi_welfare, 2))
print("time: ", gurobi_end_time - gurobi_start_time)
print()

parallelized_gurobi = Parallelized_Gurobi_Mip(
    buyer_valuations=ex["buyer_valuations"],
    seller_valuations=ex["seller_valuations"],
    seller_fixed_costs=ex["seller_fixed_costs"],
    buyer_bounds=ex["buyer_bounds"],
    seller_bounds=ex["seller_bounds"],
    seller_min_uptimes=ex["seller_min_uptimes"]
)
parallelized_gurobi_start_time = time.perf_counter()
parallelized_gurobi_solution = parallelized_gurobi.solve()
parallelized_gurobi_end_time = time.perf_counter()
parallelized_gurobi_welfare = welfare(parallelized_gurobi_solution, ex)
print("Parallelized Gurobi Solution:")
print("welfare:", np.round(parallelized_gurobi_welfare, 2))
print("time: ", parallelized_gurobi_end_time - parallelized_gurobi_start_time)
print()

admm = Admm_Mip(
    buyer_valuations=ex.buyer_valuations,
    seller_valuations=ex.seller_valuations,
    seller_fixed_costs=ex.seller_fixed_costs,
    buyer_bounds=ex.buyer_bounds,
    seller_bounds=ex.seller_bounds,
    seller_min_uptimes=ex.seller_min_uptimes,
    max_iterations=30
)
admm_start_time = time.perf_counter()
admm_solution = admm.solve()
admm_end_time = time.perf_counter()
admm_welfare = welfare(admm_solution, ex)
print("ADMM Solution:")
print("welfare:", np.round(admm_welfare, 2))
print("time: ", admm_end_time - admm_start_time)
print()

print("Comparison to Exact Gurobi:")
print("welfare %: ", np.round(100 * admm_welfare / gurobi_welfare, 2))
print("time %: ", np.round(100 * (admm_end_time - admm_start_time) / (gurobi_end_time - gurobi_start_time)))
print("Comparison to Parallelized Gurobi:")
print("welfare %: ", np.round(100 * admm_welfare / parallelized_gurobi_welfare, 2))
print("time %: ", np.round(100 * (admm_end_time - admm_start_time) / (parallelized_gurobi_end_time - parallelized_gurobi_start_time)))
print()

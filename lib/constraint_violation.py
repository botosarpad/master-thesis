import numpy as np
from examples.dotdict import dotdict
from lib.flatten import flatten


def get_simple_bounds(example):
    bounds = np.concatenate((np.array(example.buyer_bounds), np.array(example.seller_bounds)))
    lower_bounds = bounds[:, 0]
    upper_bounds = bounds[:, 1]
    return lower_bounds, upper_bounds


def get_bounds(example):
    buyer_lower_bounds = np.array(example.buyer_bounds)[:, 0]
    buyer_upper_bounds = np.array(example.buyer_bounds)[:, 1]
    seller_lower_bounds = np.array(list(map(lambda b: b[0], example.seller_bounds)))
    seller_upper_bounds = np.array(list(map(lambda b: b[1], example.seller_bounds)))
    segment_sizes = list(map(lambda b: np.array(b[2]), example.seller_bounds))
    segment_counts = np.array(list(map(lambda b: len(b[2]), example.seller_bounds)))
    return dotdict({
        "buyer_lower": buyer_lower_bounds,
        "buyer_upper": buyer_upper_bounds,
        "seller_lower": seller_lower_bounds,
        "seller_upper": seller_upper_bounds,
        "segment_sizes": segment_sizes,
        "segment_counts": segment_counts
    })


def count_simple_violated_constraints(solver_result, example):
    lower_bounds, upper_bounds = get_simple_bounds(example)
    return np.sum(~np.logical_or(np.isclose(solver_result, 0, atol=0.001),
                                 np.logical_and(np.round(solver_result, decimals=3) >= lower_bounds,
                                                np.round(solver_result, decimals=3) <= upper_bounds
                                                )
                                 ))


def single_hour_count_violated_constraints(solver_result, example):
    bounds = get_bounds(example)
    lower_bounds = np.concatenate((bounds.buyer_lower, np.zeros(len(flatten(bounds.segment_sizes)))))
    upper_bounds = np.concatenate((bounds.buyer_upper, flatten(bounds.segment_sizes)))
    violation_count = np.sum(~np.logical_or(np.isclose(solver_result, 0, atol=0.001),
                                            np.logical_and(np.round(solver_result, decimals=3) >= lower_bounds,
                                                           np.round(solver_result, decimals=3) <= upper_bounds
                                                           )
                                            ))
    segment_productions = solver_result[len(bounds.buyer_lower):]
    current_seller = 0
    current_seller_production = 0
    current_seller_segments = 0
    for i in range(len(segment_productions)):
        current_seller_production += segment_productions[i]
        current_seller_segments += 1
        if current_seller_segments == bounds.segment_counts[current_seller]:
            if not np.isclose(current_seller_production, 0, atol=0.01) and (np.round(current_seller_production, decimals=2) < bounds.seller_lower[current_seller]
                                                                            or np.round(current_seller_production, decimals=2) > bounds.seller_upper[current_seller]):
                violation_count += 1
            current_seller += 1
            current_seller_production = 0
            current_seller_segments = 0
    return violation_count


def count_violated_constraints(solver_result, example):
    count = 0
    for hour in range(len(solver_result)):
        count += single_hour_count_violated_constraints(solver_result[hour], dotdict({
            "seller_bounds": example.seller_bounds[hour],
            "buyer_bounds": example.buyer_bounds[hour]
        }))
    return count


def simple_constraint_violation_extent(solver_result, example):
    lower_bounds, upper_bounds = get_simple_bounds(example)
    violation_extent = np.zeros(len(solver_result))
    for i in range(len(solver_result)):
        if np.isclose(solver_result[i], 0, atol=0.001):
            continue
        if solver_result[i] < lower_bounds[i]:
            violation_extent[i] = solver_result[i] - lower_bounds[i]
        elif upper_bounds[i] < solver_result[i]:
            violation_extent[i] = solver_result[i] - upper_bounds[i]
    return violation_extent


def total_simple_constraint_violation_extent(solver_result, example):
    return np.sum(np.absolute(constraint_violation_extent(solver_result, example)))


def single_hour_total_constraint_violation_extent(solver_result, example):
    bounds = get_bounds(example)
    buyer_count = len(bounds.buyer_lower)
    lower_bounds = np.concatenate((bounds.buyer_lower, np.zeros(len(flatten(bounds.segment_sizes)))))
    upper_bounds = np.concatenate((bounds.buyer_upper, flatten(bounds.segment_sizes)))

    total_violation_extent = 0
    for i in range(len(solver_result)):
        if solver_result[i] < lower_bounds[i]:
            total_violation_extent += lower_bounds[i] - solver_result[i]
        if solver_result[i] > upper_bounds[i]:
            total_violation_extent += solver_result[i] - upper_bounds[i]

    segment_productions = solver_result[buyer_count:]
    current_seller = 0
    current_seller_production = 0
    current_seller_segments = 0
    for i in range(len(segment_productions)):
        current_seller_production += segment_productions[i]
        current_seller_segments += 1
        if current_seller_segments == bounds.segment_counts[current_seller]:
            if not np.isclose(current_seller_production, 0, atol=0.01):
                if current_seller_production < bounds.seller_lower[current_seller]:
                    total_violation_extent += bounds.seller_lower[current_seller] - current_seller_production
                if current_seller_production > bounds.seller_upper[current_seller]:
                    total_violation_extent += current_seller_production - bounds.seller_upper[current_seller]
            current_seller += 1
            current_seller_production = 0
            current_seller_segments = 0

    return total_violation_extent


def total_constraint_violation_extent(solver_result, example):
    tcve = 0
    for hour in range(len(solver_result)):
        tcve += single_hour_total_constraint_violation_extent(solver_result[hour], dotdict({
            "seller_bounds": example.seller_bounds[hour],
            "buyer_bounds": example.buyer_bounds[hour]
        }))
    return tcve


def get_single_hour_seller_productions(solver_result, example):
    bounds = get_bounds(example)
    buyer_count = len(bounds.buyer_lower)
    current_seller = 0
    current_seller_production = 0
    current_seller_segments = 0
    seller_productions = []
    for i in range(buyer_count, len(solver_result)):
        current_seller_production += solver_result[i]
        current_seller_segments += 1
        if current_seller_segments == bounds.segment_counts[current_seller]:
            seller_productions.append(current_seller_production)
            current_seller += 1
            current_seller_production = 0
            current_seller_segments = 0
    return seller_productions


def get_seller_productions(solver_result, example):
    seller_productions_per_hour = []
    for hour in range(len(solver_result)):
        seller_productions_per_hour.append(get_single_hour_seller_productions(solver_result[hour], dotdict({
            "seller_bounds": example.seller_bounds[hour],
            "buyer_bounds": example.buyer_bounds[hour]
        })))
    return seller_productions_per_hour


def count_violated_temporal_constraints(solver_result, example):
    seller_productions = get_seller_productions(solver_result, example)
    seller_count = len(example.seller_valuations[0])
    buyer_counts = list(map(lambda bv: len(bv), example.buyer_valuations))
    violation_count = 0
    for seller in range(seller_count):
        current_active_hours = 0
        for hour in range(len(solver_result)):
            if np.isclose(seller_productions[hour][seller], 0, atol=0.1):
                if 0 < current_active_hours and current_active_hours < example.seller_min_uptimes[seller]:
                    # print(seller, example.seller_min_uptimes[seller])
                    # production_profile = [0 if np.isclose(seller_productions[hour][seller], 0, atol=0.1) else 1
                    #                       for hour in range(len(solver_result))]
                    # print(production_profile)
                    # print()
                    violation_count += 1
                current_active_hours = 0
            else:
                current_active_hours += 1

    return violation_count

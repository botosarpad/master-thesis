import numpy as np
from lib.flatten import flatten
from examples.dotdict import dotdict


def averaged_segment_welfare(solver_result, example):
    buyer_count = len(example.buyer_valuations)
    x, y = solver_result[:buyer_count], solver_result[buyer_count:]
    z = ~np.isclose(y, 0)
    return (np.sum(example.buyer_valuations * x)
            - np.sum(example.seller_valuations * y)
            - np.sum(example.seller_fixed_costs * z))


def single_hour_welfare(solver_result, example, z_tolerance=0.001):
    buyer_count = len(example.buyer_valuations)
    seller_count = len(example.seller_valuations)
    x, y = solver_result[:buyer_count], solver_result[buyer_count:]
    z = np.zeros(seller_count)
    current_first_index = 0
    for i in range(seller_count):
        segment_count = len(example.seller_valuations[i])
        z[i] = ~np.isclose(np.sum(y[current_first_index:current_first_index+segment_count]), 0, atol=z_tolerance)
        current_first_index += segment_count
    return (np.sum(example.buyer_valuations * x)
            - np.sum(flatten(example.seller_valuations) * y)
            - np.sum(example.seller_fixed_costs * z))


def welfare(solver_result, example, z_tolerance=0.001):
    welfare = 0
    for hour in range(len(solver_result)):
        welfare += single_hour_welfare(solver_result[hour], dotdict({
            "buyer_valuations": example.buyer_valuations[hour],
            "seller_valuations": example.seller_valuations[hour],
            "seller_fixed_costs": example.seller_fixed_costs[hour]
        }), z_tolerance)
    return welfare
